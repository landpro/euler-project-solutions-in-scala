package com.lap.euler.t02

object FibonacciNumbers {

  def sumOfEvenFibonacciNumbersUpTo(n: Int) : Int = fib(1, 2, n, 0)

  private def fib(a: Int, b: Int, max: Int, sum: Int): Int = {
    if (a > max) sum else fib(b, a + b, max, if (b % 2 == 0) sum + b else sum)
  }

}
