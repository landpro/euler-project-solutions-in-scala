package com.lap.euler.t03

object LargestPrimeFactor {

  def calculate(n: Long): Long = calc(n, sqrt(n), 2, 0)

  private def calc(n: Long, nsqrt: Long, i: Long, maxFactor: Long): Long = {
    if (n % i == 0) calc(n / i, sqrt(n / i), i, i)
    else if (i > nsqrt) n else calc(n, nsqrt, i + 1, maxFactor)
  }

  def sqrt(n: Long) = Math.sqrt(n).toLong

}
