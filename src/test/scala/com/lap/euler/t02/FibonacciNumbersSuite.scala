package com.lap.euler.t02

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class FibonacciNumbersSuite extends FunSuite {

  test("sum of even fibonacci numbers up to 10") {
    assert(10 === FibonacciNumbers.sumOfEvenFibonacciNumbersUpTo(10))
  }

  test("sum of even fibonacci numbers up to 34") {
    assert(44 === FibonacciNumbers.sumOfEvenFibonacciNumbersUpTo(34))
  }

  test("sum of even fibonacci numbers up to four million") {
    assert(4613732 === FibonacciNumbers.sumOfEvenFibonacciNumbersUpTo(4000000))
  }

}
