package com.lap.euler.t03

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class LargestPrimeFactorSuite extends FunSuite {

  test("largest prime factor for prime number (29)") {
    assert(29 === LargestPrimeFactor.calculate(29))
  }

  test("largest prime factor for 1222705") {
    assert(47 === LargestPrimeFactor.calculate(1222705))
  }

  test("largest prime factor for 13195") {
    assert(29 === LargestPrimeFactor.calculate(13195))
  }

  test("largest prime factor for 600851475143 is 6857") {
    assert(6857 === LargestPrimeFactor.calculate(600851475143L))
  }

  test("square root of 25 is 5") {
    assert(5 === LargestPrimeFactor.sqrt(25))
  }

  test("square root of 26 is 5 as well") {
    assert(5 === LargestPrimeFactor.sqrt(26))
  }

}
